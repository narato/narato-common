﻿namespace Narato.Common.Models
{
    public class FeedbackItem
    {
        public FeedbackType Type { get; set; }
        public string Description { get; set; }
    }
}